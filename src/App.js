import logo from "./logo.svg";
import "./App.css";
import HatchHeaderScene from "./component/HatchHeaderScene";

function App() {
  return <HatchHeaderScene />;
}

export default App;
